CREATE DATABASE Mantenimiento_a_maquinas_industriales

/* TABLE TECNICOS */
CREATE TABLE TECNICOS(
    Numero INT PRIMARY KEY AUTO_INCREMENT,
    NomPila VARCHAR(30) NOT NULL,
    PrimApell VARCHAR(30) NOT NULL,
    SegApell VARCHAR(30),
    NumTel VARCHAR(15) NOT NULL,
    Correo VARCHAR(60),
    Usuario VARCHAR(60),
    Contraseña VARCHAR(20)
)

/* TABLE CLIENTES */
CREATE TABLE CLIENTES(
    Numero INT PRIMARY KEY AUTO_INCREMENT,
    Nombre VARCHAR(50) NOT NULL,
    NumTel VARCHAR(15) NOT NULL,
    ContNomPila VARCHAR(30) NOT NULL,
    ContPrimApell VARCHAR(30) NOT NULL,
    ContSegApell VARCHAR(30),
    Usuario VARCHAR(60),
    Contraseña VARCHAR(20)
)

/* TABLE TIPOS_MAQUINAS */
CREATE TABLE TIPOS_MAQUINAS(
    Codigo VARCHAR(5) PRIMARY KEY,
    Nombre VARCHAR(30) NOT NULL,
    Descripcion VARCHAR(100) NOT NULL
)

/* TABLE CAMBIOS */
CREATE TABLE CAMBIOS(
    Codigo VARCHAR(5) PRIMARY KEY,
    Descripcion VARCHAR(200) NOT NULL
)


/* TABLE MARCAS */
CREATE TABLE MARCAS(
    Codigo VARCHAR(5) PRIMARY KEY,
    Nombre VARCHAR(30) NOT NULL
)

/* TABLE MODELOS */
CREATE TABLE MODELOS(
    Codigo VARCHAR(5) PRIMARY KEY,
    Nombre VARCHAR(30) NOT NULL,
    Año INT,
    Marca VARCHAR(5) NOT NULL
)

-- Relacionamos MODELOS con MARCA
ALTER TABLE MODELOS
ADD CONSTRAINT FK_MODELOS_MARCAS
FOREIGN KEY(Marca) REFERENCES MARCAS(Codigo)

/* TABLE MAQUINAS */
CREATE TABLE MAQUINAS(
    Codigo VARCHAR(5) PRIMARY KEY,
    Descripcion VARCHAR(100),
    Tipo_Maquina VARCHAR(5) NOT NULL,
    Marca VARCHAR(5) NOT NULL,
    Modelo VARCHAR(5) NOT NULL
)

-- Relacionamos MAQUINAS con TIPOS_MAQUINAS
ALTER TABLE MAQUINAS
ADD CONSTRAINT FK_MAQUINAS_TIPOS_MAQUINAS
FOREIGN KEY(Tipo_Maquina) REFERENCES TIPOS_MAQUINAS(Codigo)

-- Relacionamos MAQUINAS con MARCAS
ALTER TABLE MAQUINAS 
ADD CONSTRAINT FK_MAQUINAS_MARCAS
FOREIGN KEY(Marca) REFERENCES MARCAS(Codigo)

--Relacionamos MAQUINAS con MODELOS
ALTER TABLE MAQUINAS
ADD CONSTRAINT FK_MAQUINAS_MODELOS
FOREIGN KEY(Modelo) REFERENCES MODELOS(Codigo)

/* TABLE CITAS */
CREATE TABLE CITAS(
    Numero INT PRIMARY KEY AUTO_INCREMENT,
    Fecha DATE NOT NULL,
    Hora VARCHAR(10) NOT NULL,
    Tecnico INT,
    Cliente INT
)

-- Relacionamos CITAS con TECNICOS
ALTER TABLE CITAS 
ADD CONSTRAINT FK_CITAS_TECNICOS
FOREIGN KEY(Tecnico) REFERENCES TECNICOS(Numero)

-- Relacionamos CITAS con CLIENTES
ALTER TABLE CITAS
ADD CONSTRAINT FK_CITAS_CLIENTES
FOREIGN KEY(Cliente) REFERENCES CLIENTES(Numero)

/* TABLE SERVICIOS */
CREATE TABLE SERVICIOS(
    Numero INT PRIMARY KEY AUTO_INCREMENT,
    Descripcion VARCHAR(200) NOT NULL,
    Maquina VARCHAR(5) NOT NULL,
    Cita INT NOT NULL
)

-- Relacionamos SERVICIOS con MAQUINAS
ALTER TABLE SERVICIOS
ADD CONSTRAINT FK_SERVICIOS_MAQUINAS
FOREIGN KEY(Maquina) REFERENCES MAQUINAS(Codigo)

-- Relacionamos SERVICIOS con CITAS
ALTER TABLE SERVICIOS
ADD CONSTRAINT FK_SERVICIOS_CITAS
FOREIGN KEY(Cita) REFERENCES CITAS(Numero)

/* TABLE SERV_CAMB */
CREATE TABLE SERV_CAMB(
    SERVICIOS INT,
    CAMBIOS VARCHAR(5),
    Cantidad INT NOT NULL
)

-- Creamos la PK de SERV_CAMB (SERVICIOS, CAMBIOS)
ALTER TABLE SERV_CAMB
ADD CONSTRAINT PK_SERV_CAMB
PRIMARY KEY(SERVICIOS, CAMBIOS)

-- RELACIONAMOS SERV_CAMB con SERVICIOS 
ALTER TABLE SERV_CAMB
ADD CONSTRAINT FK_SERV_CAMB_SERVICIOS
FOREIGN KEY(SERVICIOS) REFERENCES SERVICIOS(Numero)

-- Relacionamos SERV_CAMB con CAMBIOS
ALTER TABLE SERV_CAMB
ADD CONSTRAINT FK_SERV_CAMB_CAMBIOS
FOREIGN KEY(CAMBIOS) REFERENCES CAMBIOS(Codigo)


/********************************************
            R E G I S T R O S 
 *******************************************/

/* TABLE TECNICOS */
INSERT INTO TECNICOS (NomPila, PrimApell, SegApell, NumTel, Correo, Usuario, Contraseña)
VALUES
('Sebastian', 'Corbalá', 'Lizarraga', '664-1234-567', 'SebastianCorbaláLizarraga@gmail.com', 'SebastianCorbaláLizarraga@gmail.com', 'Sebastian1'),
('José Enrique', 'Flores', 'Huerta', '664-2234-567', 'JoséEnriqueFloresHuerta@gmail.com', 'JoséEnriqueFloresHuerta@gmail.com', 'Jose2'),
('Gabriel Armando', 'Gómez', 'Ramírez', '664-3234-567', 'GabrielArmandoGómezRamírez@gmail.com', 'GabrielArmandoGómezRamírez@gmail.com', 'Gabriel3'),
('Gerardo','Lazcano', 'Aguilar', '664-4234-567', 'GerardoLazcanoAguilar@gmail.com', 'GerardoLazcanoAguilar@gmail.com', 'Gerardo4'),
('Jaime Isaac', 'López' , 'Guerrero', '664-5234-567', 'JaimeIsaacLópezGuerrero@gmail.com', 'JaimeIsaacLópezGuerrero@gmail.com', 'Jaime5'),
('Eduardo Jahmir', 'López', 'Ocaña', '664-6234-567', 'EduardoJahmirLópezOcaña@gmail.com', 'EduardoJahmirLópezOcaña@gmail.com', 'Eduardo6'),
('José Sebastian', 'Lugo', 'Villa', '664-7234-567', 'JoséSebastianLugoVilla@gmail.com', 'JoséSebastianLugoVilla@gmail.com', 'José7'),
('Hugo', 'Macareno', 'Rivera', '664-8234-567', 'HugoMacarenoRivera@gmail.com', 'HugoMacarenoRivera@gmail.com', 'Hugo8'),
('Kevin Antonio', 'Mendoza', 'Gómez', '664-9234-567', 'KevinAntonioMendozaGómez@gmail.com', 'KevinAntonioMendozaGómez@gmail.com', 'Kevin9'),
('Jonathan Armando', 'Molinero', 'Perez', '664-1023-567', 'JonathanArmandoMolineroPerez@gmail.com', 'JonathanArmandoMolineroPerez@gmail.com', 'Jonathan10'),
('Jesús Esteban', 'Olmos', 'Labastida', '664-1123-567', 'JesúsEstebanOlmosLabastida@gmail.com', 'JesúsEstebanOlmosLabastida@gmail.com', 'Jesús11'),
('Axel Yahir', 'Rojero', 'Flores', '664-1223-567', 'AxelYahirRojeroFlores@gmail.com', 'AxelYahirRojeroFlores@gmail.com', 'Axel12'),
('Edgar Misael', 'Rubio', 'Zuñiga', '664-1323-567', 'EdgarMisaelRubioZuñiga@gmail.com', 'EdgarMisaelRubioZuñiga@gmail.com', 'Edgar13'),
('Daniel', 'Sotelo', 'Salinas', '664-1423-567', 'DanielSoteloSalinas@gmail.com', 'DanielSoteloSalinas@gmail.com', 'Daniel14'),
('Jonathan Giovani', 'Sanchez', 'Librado', '664-1523-567', 'JonathanGiovaniSanchezLibrado@gmail.com', 'JonathanGiovaniSanchezLibrado@gmail.com', 'Jonathan15');

/* TABLE CLIENTES */
INSERT INTO CLIENTES (Nombre, NumTel, ContNomPila, ContPrimApell, ContSegApell, Usuario, Contraseña)
VALUES
('MaquinariaPro', '664-111-2222', 'Alberto', 'Garcia', 'Lara', 'MaquinariaPro@gmail.com', 'Alberto1'),
('IndustrialTech Solutions', '664-333-4444', 'Silvia', 'Hernandez', 'Santiago', 'IndustrialTechSolutions@gmail.com', 'Silvia2'),
('Automatización Avanzada SA', '664-555-6666', 'Alejandra', 'Lara', 'Hernandez', 'AutomatizaciónAvanzadaSA@gmail.com', 'Alejandra3'),
('Máquinas del Futuro Ltda.', '664-777-8888', 'Maria', 'Escobar', 'Perez', 'MáquinasdelFuturoLtda.@gmail.com', 'Maria4'),
('Ingeniería Mecánica Integral', '664-999-0000', 'Edgar', 'Moreno', 'Lopez', 'IngenieríaMecánicaIntegral@gmail.com', 'Edgar5'),
('Innovación Industrial Global', '664-222-3333', 'Ulises', 'Ibarra', 'Torres', 'InnovaciónIndustrialGlobal@gmail.com', 'Ulises6'),
('Maquinaria Élite', '664-444-5555', 'Carlos', 'Torres', 'Perez', 'MaquinariaÉlite@gmail.com', 'Carlos7'),
('Soluciones de Automatización Industrial', '664-666-7777', 'Harry', 'Obrador', 'Garcia', 'SolucionesdeAutomatizaciónIndustrial@gmail.com', 'Harry8'),
('Tecnomaq Innovations', '664-888-9999', 'Louis', 'Cano', 'Valdez', 'TecnomaqInnovations@gmail.com', 'Louis9'),
('MaquinariaTech Corporation', '664-246-8135', 'Noemi', 'Gutierrez', 'Lara', 'MaquinariaTechCorporation@gmail.com', 'Noemi10'),
('Automatización Progresiva SA', '664-135-7924', 'Jessica', 'Espinoza', 'Gomez', 'AutomatizaciónProgresivaSA@gmail.com', 'Jessica11'),
('Ingeniería Mecatrónica Avanzada', '664-975-3102', 'Manuel', 'Sepulveda', 'Anaya', 'IngenieríaMecatrónicaAvanzada@gmail.com', 'Manuel12'),
('Maquinaria y Tecnología Industrial', '664-864-2097', 'Lupe', 'Jarquin', 'Perez', 'MaquinariayTecnologíaIndustrial@gmail.com', 'Lupe13'),
('Máquinas Inteligentes SRL', '664-321-0987', 'Lucas', 'Ramirez', 'Nata', 'MáquinasInteligentesSRL@gmail.com', 'Lucas14'),
('Automatización PrecisionTech', '664-234-5678', 'Sebastian', 'Chavarria', 'Cantor', 'AutomatizaciónPrecisionTech@gmail.com', 'Sebastian15'),
('Innovación en Maquinaria Industrial Ltd.', '664-890-1234', 'Daniel', 'Yanez', 'Guinonez', 'InnovaciónenMaquinariaIndustrialLtd.@gmail.com', 'Daniel16'),
('Avance Mecánico Global', '664-543-2109', 'Karla', 'Mesa', 'Rodriguez', 'AvanceMecánicoGlobal@gmail.com', 'Karla17'),
('Ingeniería de Maquinaria Moderna', '664-726-3849', 'Armando', 'Lopez', 'Cana', 'IngenieríadeMaquinariaModerna@gmail.com', 'Armando18'),
('MaqInnova Solutions', '664-543-2110', 'Karla', 'Patricia', 'Reyes', 'MaqInnovaSolutions@gmail.com', 'Karla19'),
('Automatización Total SA', '664-726-3850', 'Franciela', 'Lopez', 'Rodriguez', 'AutomatizaciónTotalSA@gmail.com', 'Franciela20');

/* TABLE TIPOS_MAQUINAS */
INSERT INTO TIPOS_MAQUINAS (Codigo, Nombre, Descripcion)
VALUES
('TORFI', 'Tornos Industriales Fijos', 'Máquinas que giran piezas de trabajo alrededor de un eje horizontal para realizar operaciones de torneado.'),
('FREFI', 'Fresadoras Fijas', 'Utilizadas para cortar y dar forma a materiales mediante el movimiento rotativo de una herramienta de corte.'),
('PREIN', 'Prensas Industriales', 'Máquinas que aplican fuerza para formar o dar forma a materiales mediante la compresión.'),
('SOLES', 'Máquinas de Soldadura Estacionarias', 'Equipos para unir materiales mediante soldadura, como máquinas de soldadura por arco o por puntos.'),
('CENME', 'Centros de Mecanizado', 'Máquinas que realizan operaciones de mecanizado, como perforación o fresado, de manera automática y precisa.'),
('SINES', 'Sierras Industriales Estacionarias', 'Utilizadas para cortar materiales como metal, madera o plástico.'),
('INPLA', 'Máquinas de Inyección Plástica', 'Equipos fijos que inyectan plástico fundido en moldes para la fabricación de productos plásticos.'),
('PULES', 'Pulidoras Estacionarias', 'Máquinas que pulen y suavizan superficies de materiales.'),
('EMBAU', 'Máquinas de Embalaje Automático', 'Equipos estacionarios que realizan procesos automáticos de embalaje y envasado.'),
('IMPIN', 'Máquinas de Impresión Industrial', 'Sistemas de impresión que se utilizan para marcar o imprimir en productos.'),
('HORIN', 'Hornos Industriales Fijos', 'Equipos estacionarios para el tratamiento térmico de materiales.'),
('COLAF', 'Máquinas de Corte Láser Fijas', 'Utilizadas para cortar materiales mediante un haz láser.');

/* TABLE CAMBIOS */
INSERT INTO CAMBIOS (Codigo, Descripcion)
VALUES
('PORTA', 'Cambio de portaherramientas.'),
('MANDR', 'Sustitución de los mandriles.'),
('RODAM', 'Reemplazo de rodamientos.'),
('FRESA', 'Cambio de fresas o cortadores.'),
('COTRA', 'Reemplazo de correas de transmisión.'),
('MATRI', 'Sustitución de matrices.'),
('SELLO', 'Cambio de sellos.'),
('ALSOL', 'Reemplazo de alambres de soldadura.'),
('ELECT', 'Reemplazo de electrodos.'),
('ANTOR', 'Mantenimiento de antorchas.'),
('HECOR', 'Cambio de herramientas de corte.'),
('HUSIL', 'Reemplazo de husillos.'),
('HOSIE', 'Sustitución de hojas de sierra.'),
('GUIAS', 'Cambio de guías.'),
('COJIN', 'Cambio de cojinetes.'),
('MOLDE', 'Reemplazo de moldes.'),
('BARRI', 'Cambio de barriles.'),
('PUNZO', 'Sustitución de punzones.'),
('RETEN', 'Cambio de retenedores.'),
('ABRAS', 'Sustitución de discos abrasivos.'),
('ALPUL', 'Cambio de almohadillas de pulido.');

/* TABLE MARCAS */
INSERT INTO MARCAS (Codigo, Nombre)
VALUES
('HAAS', 'Haas Automation'),
('MAZAK', 'Mazak'),
('DMG', 'DMG MORI'),
('TRUMP', 'Trumpf'),
('AMADA', 'Amada'),
('OKUMA', 'Okuma'),
('DOOSA', 'Doosan Machine Tools'),
('FANUC', 'FANUC'),
('SIEME', 'Siemens'),
('BOSCH', 'Bosch Rexroth'),
('KUKA', 'Kuka'),
('ATLAS', 'Atlas Copco'),
('ROCKA', 'Rockwell Automation'),
('LINEL', 'Lincoln Electric'),
('HELER', 'Heller'),
('HWACH', 'Hwacheon'),
('JCB', 'JCB'),
('DEWAL', 'DeWalt');

/* TABLE MODELOS */
INSERT INTO MODELOS (Codigo, Nombre, Año, Marca)
VALUES
('VF2SS', 'Haas VF-2SS', 2012, 'HAAS'),
('ST10Y', 'Haas ST-10Y', 2020, 'HAAS'),
('EC400', 'Haas EC-400', 2014, 'HAAS'),
('I200S', 'Mazak INTEGREX i-200S', 2011, 'MAZAK'),
('VTC3C', 'Mazak VTC-300C', 2021, 'MAZAK'),
('TURN2', 'Mazak QUICK TURN 200', 2017, 'MAZAK'),
('DMU50', 'DMU 50', 2018, 'DMG'),
('SY700', 'NLX 2500 SY/700', 2010, 'DMG'),
('125FD', 'DMC 125 FD duoBLOCK', 2011, 'DMG'),
('TRULA', 'TruLaser 3030', 2014, 'TRUMP'),
('TRUBE', 'TruBend 5130', 2022, 'TRUMP'),
('TRUMA', 'TruMatic 1000 fiber', 2010, 'TRUMP'),
('EMKNT', 'EMK 3610 NT', 2015, 'AMADA'),
('HGARS', 'HG 1003 ARs', 2013, 'AMADA'),
('LCGAJ', 'LCG 3015 AJ', 2016, 'AMADA'),
('GENOS', 'GENOS M560-V', 2018, 'OKUMA'),
('MULTU', 'MULTUS U4000' , 2018, 'OKUMA'),
('LB3EX', 'LB3000 EX II', 2018, 'OKUMA'),
('PUMA2', 'PUMA 2600 SY II', 2022, 'DOOSA'),
('DNM45', 'DNM 4500', 2014, 'DOOSA'),
('MX21S', 'MX 2100ST', 2022, 'DOOSA'),
('MODEF', 'FANUC Series 0i-MODEL F', 2016, 'FANUC'),
('R2000', 'FANUC R-2000iC', 2014, 'FANUC'),
('S840D', 'Sinumerik 840D', 2020, 'SIEME'),
('SIMS7', 'Simatic S7-1500', 2015, 'SIEME'),
('INMTX', 'IndraMotion MTX'	, 2013, 'BOSCH'),
('REXVA', 'Rexroth VarioFlow plus', 2021, 'BOSCH'),
('R3200', 'KR 240 R3200 ULTRA', 2022, 'KUKA'),
('AGILU', 'Kuka KR AGILUS', 2010, 'KUKA'),
('GAVSD', 'GA VSD+', 2020, 'ATLAS'),
('ZT22', 'ZT 22', 2016, 'ATLAS'),
('LOGIX', 'ControlLogix 1756-L73', 2021, 'ROCKA'),
('FLEX7', 'PowerFlex 755', 2016, 'ROCKA'),
('MIGMP', 'Power MIG 210 MP', 2020, 'LINEL'),
('TIG22', 'Precision TIG 225', 2011, 'LINEL'),
('HMC60', 'HMC 600', 2022, 'HELER'),
('FT400', 'FT 4000', 2022, 'HELER'),
('C2CNC', 'C2 CNC', 2018, 'HWACH'),
('VESTA', 'VESTA-850D', 2015, 'HWACH'),
('3CX', '3CX', 2020, 'JCB'),
('220X', '220X', 2013, 'JCB'),
('DCD79', 'DCD791D2', 2010, 'DEWAL'),
('DWE64', 'DWE6423K', 2019, 'DEWAL');

/* TABLE MAQUINAS */
INSERT INTO MAQUINAS (Codigo, Descripcion, Tipo_Maquina, Marca, Modelo)
VALUES
('M1', 'Máquina marca Haas Automation modelo Haas VF-2SS del año 2012', 'TORFI', 'HAAS', 'VF2SS'),
('M2', 'Máquina marca Haas Automation modelo Haas ST-10Y del año 2020', 'FREFI', 'HAAS', 'ST10Y'),
('M3', 'Máquina marca Haas Automation modelo Haas EC-400 del año 2014', 'PREIN', 'HAAS', 'EC400'),
('M4', 'Máquina marca Mazak modelo Mazak INTEGREX i-200S del año 2011', 'SOLES', 'MAZAK', 'I200S'),
('M5', 'Máquina marca Mazak modelo Mazak VTC-300C del año 2021', 'CENME', 'MAZAK', 'VTC3C'),
('M6', 'Máquina marca Mazak modelo Mazak QUICK TURN 200 del año 2017', 'SINES', 'MAZAK', 'TURN2'),
('M7', 'Máquina marca DMG MORI modelo DMU 50 del año 2018', 'INPLA', 'DMG', 'DMU50'),
('M8', 'Máquina marca DMG MORI modelo NLX 2500 SY/700 del año 2010', 'PULES', 'DMG', 'SY700'),
('M9', 'Máquina marca DMG MORI modelo DMC 125 FD duoBLOCK del año 2011', 'EMBAU', 'DMG', '125FD'),
('M10', 'Máquina marca Trumpf modelo TruLaser 3030 del año 2014', 'IMPIN', 'TRUMP', 'TRULA'),
('M11', 'Máquina marca Trumpf modelo TruBend 5130 del año 2022', 'HORIN', 'TRUMP', 'TRUBE'),
('M12', 'Máquina marca Trumpf modelo TruMatic 1000 fiber del año 2010', 'COLAF', 'TRUMP', 'TRUMA'),
('M13', 'Máquina marca Amada modelo EMK 3610 NT del año 2015', 'TORFI', 'AMADA', 'EMKNT'),
('M14', 'Máquina marca Amada modelo HG 1003 ARs del año 2013', 'FREFI', 'AMADA', 'HGARS'),
('M15', 'Máquina marca Amada modelo LCG 3015 AJ del año 2016', 'PREIN', 'AMADA', 'LCGAJ'),
('M16', 'Máquina marca Okuma modelo GENOS M560-V del año 2018', 'SOLES', 'OKUMA', 'GENOS'),
('M17', 'Máquina marca Okuma modelo MULTUS U4000  del año 2018', 'CENME', 'OKUMA', 'MULTU'),
('M18', 'Máquina marca Okuma modelo LB3000 EX II del año 2018', 'SINES', 'OKUMA', 'LB3EX'),
('M19', 'Máquina marca Doosan Machine Tools modelo PUMA 2600 SY II del año 2022', 'INPLA', 'DOOSA', 'PUMA2'),
('M20', 'Máquina marca Doosan Machine Tools modelo DNM 4500 del año 2014', 'PULES', 'DOOSA' ,'DNM45'),
('M21', 'Máquina marca Doosan Machine Tools modelo MX 2100ST del año 2022', 'EMBAU', 'DOOSA', 'MX21S'),
('M22', 'Máquina marca FANUC modelo FANUC Series 0i-MODEL F del año 2016', 'IMPIN', 'FANUC', 'MODEF'),
('M23', 'Máquina marca FANUC modelo FANUC R-2000iC del año 2014', 'HORIN', 'FANUC', 'R2000'),
('M24', 'Máquina marca Siemens modelo Sinumerik 840D del año 2020', 'COLAF', 'SIEME', 'S840D'),
('M25', 'Máquina marca Siemens modelo Simatic S7-1500 del año 2015', 'TORFI', 'SIEME', 'SIMS7'),
('M26', 'Máquina marca Bosch Rexroth modelo IndraMotion MTX del año 2013', 'FREFI', 'BOSCH', 'INMTX'),
('M27', 'Máquina marca Bosch Rexroth modelo Rexroth VarioFlow plus del año 2021', 'PREIN', 'BOSCH', 'REXVA'),
('M28', 'Máquina marca Kuka modelo KR 240 R3200 ULTRA del año 2022', 'SOLES', 'KUKA', 'R3200'),
('M29', 'Máquina marca Kuka modelo Kuka KR AGILUS del año 2010', 'CENME', 'KUKA', 'AGILU'),
('M30', 'Máquina marca Atlas Copco modelo GA VSD+ del año 2020', 'SINES', 'ATLAS', 'GAVSD'),
('M31', 'Máquina marca Atlas Copco modelo ZT 22 del año 2016', 'INPLA', 'ATLAS', 'ZT22'),
('M32', 'Máquina marca Rockwell Automation modelo ControlLogix 1756-L73 del año 2021', 'PULES',	'ROCKA', 'LOGIX'),
('M33', 'Máquina marca Rockwell Automation modelo PowerFlex 755 del año 2016', 'EMBAU', 'ROCKA', 'FLEX7'),
('M34', 'Máquina marca Lincoln Electric modelo Power MIG 210 MP del año 2020', 'IMPIN', 'LINEL', 'MIGMP'),
('M35', 'Máquina marca Lincoln Electric modelo Precision TIG 225 del año 2011', 'HORIN', 'LINEL', 'TIG22'),
('M36', 'Máquina marca Heller modelo HMC 600 del año 2022', 'COLAF', 'HELER', 'HMC60'),
('M37', 'Máquina marca Heller modelo FT 4000 del año 2022', 'TORFI', 'HELER', 'FT400'),
('M38', 'Máquina marca Hwacheon modelo C2 CNC del año 2018', 'FREFI', 'HWACH', 'C2CNC'),
('M39', 'Máquina marca Hwacheon modelo VESTA-850D del año 2015', 'PREIN', 'HWACH', 'VESTA'),
('M40', 'Máquina marca JCB modelo 3CX del año 2020', 'SOLES', 'JCB', '3CX'),
('M41', 'Máquina marca JCB modelo 220X  del año 2013', 'CENME', 'JCB', '220X'),
('M42', 'Máquina marca DeWalt modelo DCD791D2 del año 2010', 'SINES', 'DEWAL', 'DCD79'),
('M43', 'Máquina marca DeWalt modelo DWE6423K del año 2019', 'INPLA', 'DEWAL', 'DWE64');

/* TABLE CITAS */
INSERT INTO CITAS (Fecha, Hora, Tecnico, Cliente)
VALUES
('2023-02-07', '08:00', 15, 1),
('2023-08-06', '12:00', 10, 4),
('2023-07-19', '12:00', 2, 1),
('2023-12-18', '18:00', 12, 18),
('2023-02-22', '12:00', 1, 3),
('2023-03-23', '09:00', 14, 7),
('2023-08-26', '13:00', 8, 11),
('2023-05-20', '09:00', 8, 4),
('2023-02-08', '14:00', 5, 5),
('2023-04-10', '11:00', 9, 5),
('2023-07-23', '10:00', 7, 7),
('2023-12-22', '11:00', 7, 2),
('2023-10-08', '11:00', 9, 16),
('2023-06-07', '14:00', 12, 18),
('2023-10-27', '10:00', 14, 2),
('2023-07-02', '10:00', 9, 15),
('2023-04-02', '14:00', 14, 14),
('2023-03-21', '16:00', 3, 11),
('2023-05-25', '08:00', 10, 12),
('2023-02-25', '14:00', 10, 18),
('2023-11-24', '18:00', 14, 6),
('2023-09-30', '17:00', 4, 1),
('2023-03-03', '19:00', 15, 2),
('2023-06-13', '16:00', 15, 8),
('2023-08-09', '09:00', 10, 14),
('2023-03-11', '11:00', 4, 3),
('2023-08-26', '07:00', 6, 6),
('2023-11-01', '17:00', 2, 3),
('2023-01-18', '19:00', 9, 19),
('2023-10-14', '11:00', 13, 8),
('2023-05-14', '19:00', 13, 9),
('2023-12-29', '19:00', 5, 9),
('2023-05-06', '17:00', 8, 16),
('2023-04-09', '17:00', 8, 10),
('2023-04-30', '14:00', 9, 20),
('2023-10-28', '18:00', 14, 10),
('2023-03-16', '14:00', 2, 12),
('2023-03-10', '07:00', 4, 13),
('2023-04-18', '13:00', 10, 17),
('2023-05-11', '18:00', 11, 19),
('2023-02-17', '18:00', 14, 15),
('2023-02-21', '13:00', 7, 13),
('2023-12-12', '11:00', 9, 20),
('2023-06-16', '15:00', 9, 20),
('2023-05-29', '07:00', 12, 17),
('2023-01-09', '10:00', 13, 10),
('2023-04-29', '13:00', 5, 16),
('2023-06-27', '19:00', 15, 10),
('2023-12-24', '07:00', 3, 7),
('2023-11-19', '14:00', 4, 16);

/* TABLE SERVICIOS */
INSERT INTO SERVICIOS (Descripcion, Maquina, Cita)
VALUES
('Se le dio limpieza a la máquina', 'M1', 1),
('Lubricación de componentes', 'M4', 2),
('Se ajustó la calibración', 'M21', 3),
('Se le dio limpieza a la máquina', 'M18', 4),
('Se le realizaron cambios de piezas', 'M3', 5),
('Se le realizaron cambios de piezas', 'M7', 6),
('Lubricación de componentes', 'M11', 7),
('Se le dio limpieza a la máquina', 'M24', 8),
('Se ajustó la calibración', 'M5', 9),
('Se le realizaron cambios de piezas', 'M25', 10),
('Se le dio limpieza a la máquina', 'M27', 11),
('Se le realizaron cambios de piezas', 'M2', 12),
('Se le dio limpieza a la máquina y lubricacion de componentes', 'M16', 13),
('Servicio de preevención o seguimiento', 'M38', 14),
('Servicio de preevención o seguimiento', 'M22', 15),
('Se le realizaron cambios de piezas', 'M15', 16),
('Se ajustó la calibración', 'M14', 17),
('Se le dio limpieza a la máquina', 'M31', 18),
('Lubricación de componentes', 'M12', 19),
('Servicio de preevención o seguimiento', 'M38', 20),
('Se ajustó la calibración', 'M6', 21),
('Se le realizaron cambios de piezas', 'M41', 22),
('Se le dio limpieza a la máquina', 'M42', 23),
('Se le realizaron cambios de piezas', 'M8', 24),
('Lubricación de componentes', 'M34', 25),
('Implementación de sistemas de monitoreo en tiempo real.', 'M23', 26),
('Se le dio limpieza a la máquina', 'M26', 27),
('Servicio de preevención o seguimiento', 'M43', 28),
('Servicio de preevención o seguimiento', 'M19', 29),
('Lubricación de componentes', 'M28', 30),
('Se le dio limpieza a la máquina', 'M9', 31),
('Se ajustó la calibración', 'M29', 32),
('Se le realizaron cambios de piezas', 'M36', 33),
('Se le realizaron cambios de piezas', 'M10', 34),
('Se le dio limpieza a la máquina', 'M20', 35),
('Servicio de preevención o seguimiento', 'M30', 36),
('Implementación de medidas para reducir el consumo de energía.', 'M32', 37),
('Servicio de preevención o seguimiento', 'M13', 38),
('Se le dio limpieza a la máquina', 'M17', 39),
('Implementación de sistemas de monitoreo en tiempo real.', 'M39', 40),
('Implementación de medidas para reducir el consumo de energía.', 'M35', 41),
('Lubricación de componentes y se ajustó la calibración', 'M33', 42),
('Servicio de preevención o seguimiento', 'M40', 43),
('Se le realizaron cambios de piezas', 'M20', 44),
('Se le dio limpieza a la máquina', 'M37', 45),
('Servicio de preevención o seguimiento', 'M10', 46),
('Se le realizaron cambios de piezas', 'M36', 47),
('Servicio de preevención o seguimiento', 'M30', 48),
('Se le dio limpieza a la máquina', 'M27', 49),
('Lubricación de componentes', 'M16', 50);

/* TABLE SERV_CAMB */
INSERT INTO SERV_CAMB (SERVICIOS, CAMBIOS, Cantidad)
VALUES
(5, 'PORTA', 3),
(6, 'RODAM', 3),
(10, 'COTRA', 8),
(12, 'SELLO', 1),
(16, 'ELECT', 5),
(22, 'HECOR', 2),
(24, 'HUSIL', 3),
(33, 'GUIAS', 1),
(34, 'MOLDE', 4),
(44, 'PUNZO', 5),
(47, 'ABRAS', 3);

/**************************************
           C O N S U L T A S
**************************************/

/* 
    1. Información del servicio de una máquina 
        a. Descripción de la máquina 
        b. Nombre del cliente 
        c. Nombre completo del contacto en una columna 
        d. Fecha de la cita 
        e. Hora de la cita 
        f. Nombre del tipo de máquina 
        g. Nombre de la marca 
        h. Nombre del modelo 
        i. Descripción del servicio 
        j. Nombre completo del técnico en una columna
*/

SELECT
MQ.Descripcion AS Maquina,
CL.Nombre AS Cliente,
CONCAT(
        IFNULL(CONCAT(CL.ContNomPila, ' '), ''),
        IFNULL(CONCAT(CL.ContPrimApell, ' '), ''),
        IFNULL(CONCAT(CL.ContSegApell, ' '), '')
    ) AS Contacto,
DATE_FORMAT(CI.Fecha, "%d-%M-%Y") AS "Fecha de la cita",
CI.HORA AS "Hora de la cita",
TM.Nombre AS "Tipo de máquina",
MA.Nombre AS Marca,
MO.Nombre AS Modelo,
S.Descripcion AS "Descripcion del servicio",
CONCAT(
        IFNULL(CONCAT(TE.NomPila, ' '), ''),
        IFNULL(CONCAT(TE.PrimApell, ' '), ''),
        IFNULL(CONCAT(TE.SegApell, ' '), '')
    ) AS Técnico
FROM MAQUINAS AS MQ
INNER JOIN TIPOS_MAQUINAS AS TM ON MQ.Tipo_Maquina = TM.Codigo
INNER JOIN MARCAS AS MA ON MQ.Marca = MA.Codigo
INNER JOIN MODELOS AS MO ON MQ.Modelo = MO.Codigo
INNER JOIN SERVICIOS AS S ON MQ.Codigo = S.Maquina
INNER JOIN CITAS AS CI ON S.Cita = CI.Numero
INNER JOIN TECNICOS AS TE ON CI.Tecnico = TE.Numero
INNER JOIN CLIENTES AS CL ON CI.Cliente = CL.Numero
WHERE MQ.Codigo = 'M7'

-- Agregamos más registros para esta consulta.
INSERT INTO CITAS (Fecha, Hora, Tecnico, Cliente)
VALUES
('2023-10-07', '10:00', 10, )



/*
    2. Cambios realizados en un servicio a una máquina 
        a. Número del servicio 
        b. Nombre del cambio 
        c. Cantidad realizada 
*/

SELECT
S.Numero AS Servicio,
CA.Descripcion AS 'Cambios realizados',
SC.Cantidad AS Cantidad
FROM SERVICIOS AS S
INNER JOIN SERV_CAMB AS SC ON S.Numero = SC.SERVICIOS
INNER JOIN CAMBIOS AS CA ON SC.CAMBIOS = CA.Codigo
INNER JOIN MAQUINAS AS MQ ON S.Maquina = MQ.Codigo
WHERE S.Numero = 5

/*
    3. Servicios realizados a una máquina 
        a. Descripción de la máquina 
        b. Nombre del cliente 
        c. Fecha de la cita 
        d. Nombre del tipo de máquina 
        e. Nombre de la marca 
        f. Nombre del modelo 
        g. Descripción del servicio 
*/

SELECT
MQ.Descripcion AS Maquina,
CL.Nombre AS Cliente,
DATE_FORMAT(CI.Fecha, "%d-%M-%Y") AS "Fecha de la cita",
TM.Nombre AS 'Tipo de maquina',
MA.Nombre AS Marca,
MO.Nombre AS Modelo,
S.Descripcion AS 'Descrpcion del servicio'
FROM MAQUINAS AS MQ
INNER JOIN TIPOS_MAQUINAS AS TM ON MQ.Tipo_Maquina = TM.Codigo
INNER JOIN MARCAS AS MA ON MQ.Marca = MA.Codigo
INNER JOIN MODELOS AS MO ON MQ.Modelo = MO.Codigo
INNER JOIN SERVICIOS AS S ON MQ.Codigo = S.Maquina
INNER JOIN CITAS AS CI ON S.Cita = CI.Numero
INNER JOIN CLIENTES AS CL ON CI.Cliente = CL.Numero
WHERE MQ.Codigo = 'M7'

/*
    4. Máquinas de un mismo cliente que ha solicitado servicios 
        a. Nombre del cliente 
        b. Descripción de la máquina 
        c. Nombre del tipo de máquina 
        d. Nombre de la marca 
        e. Nombre del modelo 
        f. Fecha de la cita 
        g. Descripción del servicio
*/

SELECT
CL.Nombre AS Cliente,
MQ.Descripcion AS Maquina,
TM.Nombre AS 'Tipo de maquina',
MA.Nombre AS Marca,
MO.Nombre AS Modelo,
S.Descripcion AS 'Descrpcion del servicio'
FROM MAQUINAS AS MQ
INNER JOIN TIPOS_MAQUINAS AS TM ON MQ.Tipo_Maquina = TM.Codigo
INNER JOIN MARCAS AS MA ON MQ.Marca = MA.Codigo
INNER JOIN MODELOS AS MO ON MQ.Modelo = MO.Codigo
INNER JOIN SERVICIOS AS S ON MQ.Codigo = S.Maquina
INNER JOIN CITAS AS CI ON S.Cita = CI.Numero
INNER JOIN CLIENTES AS CL ON CI.Cliente = CL.Numero
WHERE CL.Numero = 1

/*
    5. Técnicos que han atendido servicios 
        a. Nombre completo del técnico en una columna 
        b. Fecha de la cita 
        c. Descripción de la máquina 
        d. Nombre del tipo de máquina 
        e. Nombre de la marca 
        f. Nombre del modelo 
        g. Descripción del servicio 
*/

SELECT
CONCAT(
        IFNULL(CONCAT(TE.NomPila, ' '), ''),
        IFNULL(CONCAT(TE.PrimApell, ' '), ''),
        IFNULL(CONCAT(TE.SegApell, ' '), '')
    ) AS Técnico,
DATE_FORMAT(CI.Fecha, "%d-%M-%Y") AS "Fecha de la cita",
MQ.Descripcion AS 'Maquina',
TM.Nombre AS 'Tipo de maquina',
MA.Nombre AS Marca,
MO.Nombre AS Modelo,
S.Descripcion AS 'Descripcion del servicio'
FROM MAQUINAS AS MQ
INNER JOIN TIPOS_MAQUINAS AS TM ON MQ.Tipo_Maquina = TM.Codigo
INNER JOIN MARCAS AS MA ON MQ.Marca = MA.Codigo
INNER JOIN MODELOS AS MO ON MQ.Modelo = MO.Codigo
INNER JOIN SERVICIOS AS S ON MQ.Codigo = S.Maquina
INNER JOIN CITAS AS CI ON S.Cita = CI.Numero
INNER JOIN TECNICOS AS TE ON CI.Tecnico = TE.Numero

/*
    6. Citas programadas para una semana en particular 
        a. Número de la cita 
        b. Fecha de la cita 
        c. Hora de la cita 
        d. Nombre del cliente 
        e. Nombre completo del contacto en una columna 
        f. Descripción de la máquina 
        g. Nombre del tipo de máquina 
        h. Nombre completo del técnico asignado en una columna
*/

SELECT
CI.Numero AS CITA,
DATE_FORMAT(CI.Fecha, "%d-%M-%Y") AS "Fecha de la cita",
CI.Hora AS 'Hora de la cita',
CL.Nombre AS Cliente,
CONCAT(
        IFNULL(CONCAT(CL.ContNomPila, ' '), ''),
        IFNULL(CONCAT(CL.ContPrimApell, ' '), ''),
        IFNULL(CONCAT(CL.ContSegApell, ' '), '')
    ) AS Contacto,
MQ.Descripcion AS Maquina,
TM.Nombre AS 'Tipo de maquina',
CONCAT(
        IFNULL(CONCAT(TE.NomPila, ' '), ''),
        IFNULL(CONCAT(TE.PrimApell, ' '), ''),
        IFNULL(CONCAT(TE.SegApell, ' '), '')
    ) AS Técnico
FROM CITAS AS CI
INNER JOIN CLIENTES AS CL ON CI.Cliente = CL.Numero
INNER JOIN TECNICOS AS TE ON CI.Tecnico = TE.Numero
INNER JOIN SERVICIOS AS S ON CI.Numero = S.Cita
INNER JOIN MAQUINAS AS MQ ON S.Maquina = MQ.Codigo
INNER JOIN TIPOS_MAQUINAS AS TM ON MQ.Tipo_Maquina = TM.Codigo
WHERE CI.Fecha BETWEEN '2023-07-01' AND '2023-07-07'

/*
    7. Cambios realizados en todos los servicios realizados a una misma máquina 
        a. Descripción de la máquina 
        b. Nombre del tipo de máquina 
        c. Fecha de la cita 
        d. Descripción del servicio 
        e. Descripción del cambio 
        f. Cantidad
*/

SELECT 
MQ.Descripcion AS Maquina,
TM.Nombre AS 'Tipo de maquina',
DATE_FORMAT(CI.Fecha, "%d-%M-%Y") AS "Fecha de la cita",
S.Descripcion AS 'Descripcion del servicio',
CA.Descripcion AS Cambio,
SC.Cantidad AS Cantidad
FROM MAQUINAS AS MQ
INNER JOIN TIPOS_MAQUINAS AS TM ON MQ.Tipo_Maquina = TM.Codigo
INNER JOIN SERVICIOS AS S ON MQ.Codigo = S.Maquina
INNER JOIN CITAS AS CI ON S.Cita = CI.Numero
INNER JOIN SERV_CAMB AS SC ON S.Numero = SC.SERVICIOS
INNER JOIN CAMBIOS AS CA ON SC.CAMBIOS = CA.Codigo
WHERE MQ.Codigo = 'M27'

/*
    8. Máquinas del mismo tipo 
        a. Nombre del tipo de máquina 
        b. Nombre de la máquina 
        c. Nombre de la marca 
        d. Nombre del modelo 
        e. Año
*/

SELECT
TM.Nombre AS 'Tipo de maquina',
MQ.Descripcion AS Maquina,
MA.Nombre AS Marca,
MO.Nombre AS Modelo,
MO.Año AS Año
FROM MAQUINAS AS MQ
INNER JOIN TIPOS_MAQUINAS AS TM ON MQ.Tipo_Maquina = TM.Codigo
INNER JOIN MARCAS AS MA ON MQ.Marca = MA.Codigo
INNER JOIN MODELOS AS MO ON MQ.Modelo = MO.Codigo
WHERE TM.Codigo = 'CENME'

/*
    9. Modelos de la misma marca 
        a. Nombre de la marca 
        b. Nombre del modelo 
        c. Año
*/

SELECT
MA.Nombre AS Marca,
MO.Nombre AS Modelo,
MO.Año AS Año
FROM MARCAS AS MA
INNER JOIN MODELOS AS MO ON MA.Codigo = MO.Marca
WHERE MA.Codigo = 'AMADA'

/*
    10. Cantidad de citas por técnico en un mes determinado 
        a. Nombre completo del técnico en una columna 
        b. Cantidad de citas 
*/

SELECT
CONCAT(
        IFNULL(CONCAT(TE.NomPila, ' '), ''),
        IFNULL(CONCAT(TE.PrimApell, ' '), ''),
        IFNULL(CONCAT(TE.SegApell, ' '), '')
    ) AS Técnico,
COUNT(CI.Numero) AS 'Cantidad de citas'
FROM CITAS AS CI
INNER JOIN TECNICOS AS TE ON CI.Tecnico = TE.Numero
WHERE MONTH(CI.Fecha) = 2 
AND YEAR(CI.Fecha) = 2023
GROUP BY TE.Numero

/*****************************************
             T R I G G E R S
*****************************************/

/*
    1. Antes de la inserción de una nueva cita: 
        a. Verificar la disponibilidad de los técnicos para esa fecha y hora. 
        b. Validar que la máquina no esté programada para otro servicio en 
        el mismo período.
*/

DELIMITER $$
CREATE TRIGGER VERIFICAR_CITA
BEFORE INSERT ON CITAS
FOR EACH ROW 
BEGIN

DELIMITER $$

CREATE TRIGGER VERIFICAR_CITA
BEFORE INSERT ON CITAS 
FOR EACH ROW
BEGIN
    DECLARE count_tecnicos INT;
    DECLARE count_maquinas INT;

    -- Verificar disponibilidad de técnicos
    SELECT COUNT(*)
    INTO count_tecnicos
    FROM tabla_disponibilidad_tecnicos
    WHERE tecnico_id = NEW.tecnico_id
      AND fecha = NEW.fecha
      AND hora = NEW.hora;

    IF count_tecnicos > 0 THEN
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'El técnico no está disponible en esta fecha y hora';
    END IF;

    -- Validar que la máquina no esté programada para otro servicio en el mismo período
    SELECT COUNT(*)
    INTO count_maquinas
    FROM tabla_programacion_maquinas
    WHERE maquina_id = NEW.maquina_id
      AND fecha = NEW.fecha
      AND hora = NEW.hora;

    IF count_maquinas > 0 THEN
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'La máquina ya tiene cita el mismo día';
    END IF;
END $$

DELIMITER ;
